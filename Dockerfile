FROM rocker/shiny-verse:3.6.1

RUN apt-get update && apt-get install -y \
	libxml2-dev \
  libgdal-dev \
  libudunits2-dev \
	libmagick++-dev && \
	R -e "install.packages(c('shinydashboard', 'conflicted', 'slickR', 'tidyverse', 'sf', 'plotly'), repos='https://cran.rstudio.com')"

# remove shiny sample
RUN rm -rf /srv/shiny-server

RUN apt-get install -y vim

COPY ./shiny-server.conf /etc/shiny-server/shiny-server.conf

# copy app files into srv directory
COPY src /srv/shiny-server
