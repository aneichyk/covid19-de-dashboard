
[[_TOC_]]

# Explore regional data from Germany

Main code to manipulate outputs is in `src/modules/`

One tab of the app corresponds to one file in this directory.

The data loading for regional maps is very slow. It seems to be that `data/gadm36_DEU_shp/gadm36_DEU_2.shp`
is rather heavy file, and lighter versions exist (I just didn't find it).




## How to set up on your machine

Install docker

https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-engine---community-1


```
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```

```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

```

```
sudo apt-key fingerprint 0EBFCD88
```

Compare the output to below:

```
pub   4096R/0EBFCD88 2017-02-22
      Key fingerprint = 9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
uid                  Docker Release (CE deb) <docker@docker.com>
sub   4096R/F273FCD8 2017-02-22

```


```
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```


```
sudo apt-get update
```


```
sudo apt-get install docker-ce docker-ce-cli containerd.io

```

Install docker-compose

```
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

```

## Build and run the app on your machine

*   clone app repository

```
git clone https://gitlab.com/idatalab/applications/covid19-de-dashboard.git
```

```
cd covid19-de-dashboard 
```

build the image

```
sudo docker-compose build
```

will take a while


Run the image:

```
sudo docker-compose up
```

Once the container is up and running, it will print http to connect to the app:

```
idl-shiny-test | [2020-03-21T19:27:01.013] [INFO] shiny-server - Starting listener on http://[::]:3838
```

go to : `http://[::]:3838`


